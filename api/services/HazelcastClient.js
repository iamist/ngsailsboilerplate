var java = require('java'),
  Promise = require('bluebird'),
  debug = require('debug');

java.asyncOptions = {
  // asyncSuffix: undefined,
  asyncSuffix: 'Async',
  syncSuffix: '',              // Sync methods use the base name(!!)
  promiseSuffix: "Promise",
  promisify: require('when/node').lift
};

java.classpath.push(process.cwd() + '/lib/hazelcast/hazelcast-3.4.4.jar');
java.classpath.push(process.cwd() + '/lib/hazelcast/hazelcast-client-3.4.4.jar');

var HazelcastClientClass = java.import('com.hazelcast.client.HazelcastClient');
var ClientConfigClass = java.import('com.hazelcast.client.config.ClientConfig');
var ArrayListClass = java.import('java.util.ArrayList');

var HazelcastClient = function() {

  var self = this instanceof HazelcastClient ? this : Object.create(HazelcastClient.prototype);

  self.clientConfig = new ClientConfigClass();
  self.addresses = new ArrayListClass();

  return self;
}

/**
 * creates a HazelcastClient instance
 * @type {[hazelcastClientInstance]}
 */
HazelcastClient.prototype.new = function() {
  var self = this;
  return new Promise(function(resolve, reject) {
    if (!self.networkConfig.getAddresses().size()) return reject({error: 'HZ_CLIENT_ERROR_MEMBER_ADDRESS_NOT_SET'});

    try {
      return resolve(HazelcastClientClass.newHazelcastClient(self.clientConfig));
    } catch(details) {
      return reject({message: 'HZ_CLIENT_UNABLE_TO_CONNECT_TO_ADDRESSES_SPECIFIED'});
    }
  });
};

/**
 * Returns an existing HazelcastClient with instanceName.
 * @param  {[string]} instanceName [description]
 * @return {[hazelcastClientInstance]}
 */
HazelcastClient.prototype.getHazelcastClientByName = function (instanceName) {
  return Promise.resolve().then(function() {
    return HazelcastClientClass.getHazelcastClientByName(instanceName);
  });
}

/**
 * add array of addresses to object address Array object
 * @param  {[array]} addresses [an array with one or more cluster member object elements]
 * @return {[HazelcastClient]}
 */
HazelcastClient.prototype.setAddresses = function(addresses) {
  var self = this,
    arrAddresses = new ArrayListClass();;
  self.networkConfig = self.clientConfig.getNetworkConfig();
  _.forEach(addresses, function(address) {
    arrAddresses.add(address.hostname + ':' + address.port);
  });

  // self.networkConfig.setSmartRouting(false);
  self.networkConfig.setConnectionAttemptLimit(1); //int
  self.networkConfig.setConnectionAttemptPeriod(5); //ms
  self.networkConfig.setAddresses(arrAddresses);
  return self;
}

/**
 * return interface Map instance by giving string map name
 * @param  {[string]} map
 * @return {[iMap]}     interfaceMap
 */
HazelcastClient.prototype.getMap = function(map) {

}

HazelcastClient.prototype.shutdownAll = function() {
  HazelcastClientClass.shutdownAll();
}

module.exports = HazelcastClient();
