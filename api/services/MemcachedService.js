var Memcached = require('memcached'),
  Promise = require('bluebird');

var MemcachedService = function(args) {
  var self = this instanceof MemcachedService ? this : Object.create(MemcachedService.prototype);
  var settings = _.merge({}, args);

  self.cache_client = false;

  if (!_.isEmpty(settings)) {
    self.connect(settings);
  }

  return self;
}

MemcachedService.prototype.connect = function(cluster, opts) {
  var self = this;
  var options = _.merge({
    retries: 1,
    retry: 600
  }, opts);

  return new Promise(function(reject, resolve) {
    self.cache_client = new Memcached([cluster.host, cluster.port].join(':'), options);

    self.cache_client.on('failure', function(details) {
      reject(new Error(details));
    });
  });
}

/*MemcachedService.prototype.serverStats = function() {
  var self = this;
  return new Promise(function(resolve, reject) {
    self.cache_client.stats(function(err, data) {
      if(err) {
        reject(err);
      } else {
         resolve(data[0]);
      }
    });
  });
}*/

module.exports = MemcachedService;
