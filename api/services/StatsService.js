var Promise = require('bluebird'),
  moment = require('moment'),
  numeral = require('numeral'),
  debug = require('debug')('APP:StatsService');

var StatsService = function(cluster) {
  var self = this instanceof StatsService ? this : Object.create( StatsService.prototype );

  self.cluster = cluster;
  self._data = false;

  return self;
}

StatsService.prototype.serverStats = function() {
  var self = this;
  return new Promise(function(resolve, reject) {
    self.cluster.cache_client.stats(function(err, data) {
      if (err) {
        reject(err)
      } else {
        self._data = data[0];
        resolve(data[0]);
      }
      self.cluster.cache_client.end();
    });
  });
}

StatsService.prototype.statistics = function() {
  return _.merge(this._data,
    {
      server_status: _serverStatus(this._data),
      get_stats: _cmdGetStats(this._data),
      set_stats: _cmdSetStats(this._data)
    });
}

function _serverStatus(data) {
  var uptime = moment.duration(data.uptime, 'seconds');
  return {
    'Uptime': uptime.days() + ' days ' + uptime.hours() + ' hours ' + uptime.minutes() + ' minutes ' + uptime.seconds() + ' seconds',
    'Memcached': 'Version ' + data.version,
    'Curr Connection': data.curr_connections,
    'Total Connection': data.total_connections,
    'Current Items': data.current_items,
    'Total Items': data.total_items
  }
}

function _cmdGetStats(data) {
  return {
    "Hits": { value: data.get_hits, percentage: data.cmd_get === 0 ? 0 : numeral((data.get_hits/data.cmd_get) * 100).format('0,0') + '%' },
    "Miss": { value: data.get_misses, percentage: data.cmd_get === 0 ? 0 : numeral((data.get_misses/data.cmd_get) * 100).format('0,0') + '%' },
    "Rate": { value: null, percentage: data.cmd_get === 0 ? 0 : numeral(data.cmd_get / data.uptime).format('0,0') + ' Request/sec' }
  }
}

function _cmdSetStats(data) {
  return {
    "Total": {value: data.cmd_set},
    "Rate": {value: data.cmd_set === 0 ? 0 : numeral(data.cmd_set / data.uptime).format('0,0') + ' Request/sec'}
  }
}

module.exports = StatsService;
