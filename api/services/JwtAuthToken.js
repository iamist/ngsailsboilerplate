var jwt = require('jsonwebtoken'),
  Promise = require('bluebird');

var jwtVerify = Promise.promisify(jwt.verify, jwt),
  serverSecret = sails.config.security.jwtSecret;

var JwtAuthToken = {
  issue: function(payload) {
    return jwt.sign(payload, serverSecret);
  },

  verify: function(token, opts) {
    return jwtVerify(token, serverSecret)
      .then(function(verified) {});
  }
}

module.exports = JwtAuthToken;
