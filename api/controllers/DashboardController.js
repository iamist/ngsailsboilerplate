/**
 * DashboardController
 *
 * @description :: Server-side logic for managing dashboards
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 var async = require('async');

 module.exports = {
  index:  function(req, res) {
    return res.view({
      ngApp: 'app.dashboard',
      pageTitle: "Dashboard"
    });
  }
};
