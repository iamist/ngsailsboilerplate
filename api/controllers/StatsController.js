/**
 * StatsController
 *
 * @description :: Server-side logic for managing Stats
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var Promise = require('bluebird'),
  debug = require('debug')('APP:StatsController');

module.exports = {
	statistics: function(req, res) {
		res.json({success: true})
	},

  memoryDistribution: function(req, res) {
    return res.json([
      {label: 'Current Connection', data: 978},
      {label: 'Total Connection', data: 92952},
      {label: 'Max Connection Errors', data: 0},
      {label: 'Current Items', data: 8002192},
      {label: 'Total Items', data: 130545454}
    ]);
  },

  cacheSize: function(req, res) {
    return res.json([
      {label: 'Used (GBytes)', data: 1.7},
      {label: 'Total (GBytes)', data: 6.0},
      {label: 'Wasted (MBytes)', data: 300}
    ])
  }

};
