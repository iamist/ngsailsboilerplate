/**
 * ClusterController
 *
 * @description :: Server-side logic for managing Clusters
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var java = require('java'),
	Promise = require('bluebird');

var hz = {
	getMap: function(hzInstance, mapName) {
		return Promise
						.resolve()
						.then(function() {
							return hzInstance.getMap(mapName);
						});
	},
	countMapEntries: function(iMap) {
		return Promise
						.resolve()
						.then(function() {
							return iMap.values().size();
						});
	},
	getLocalMapStats: function(iMap) {
		return Promise
						.resolve()
						.then(function() {
							return iMap.getLocalMapStats();
						});
	},
	addressFormat: function(address) {
		if (!address) return false;

		var addr = address.split(':');
		var data = {
			hostname: addr[0]
		};
		if (addr.length > 1) {
			_.merge(data, {port: addr[1]});
		}

		return data;

	}
};

module.exports = {
	checkStatus: function(req, res) {
		var values, objHzClientInstance, address = [];

		if (!req.param('member')) {
			return res.badRequest({details: 'HZ_MEMBER_ADDRESS_NOT_SPECIFIED'});
		}

		if (_.isArray(req.param('member')) > 1) {
			address = _.map(req.param('member'), hz.addressFormat);
		} else {
			address.push(hz.addressFormat(req.param('member')));
		}

		HazelcastClient
			.setAddresses(address)
			.new()
			.then(function(hzClientInstance) {
				objHzClientInstance = hzClientInstance;
				return hz.getMap(hzClientInstance, 'default');
			})
			.then(function(iMap) {
				return [hz.countMapEntries(iMap), hz.getLocalMapStats(iMap)];
			})
			.spread(function(intMapEntries, objLocalMapStats, iMap) {
				values = {
					entries: intMapEntries,
					entryMemory: objLocalMapStats.getOwnedEntryMemoryCost,
					backups: objLocalMapStats.getBackupCount(),
					backupMemory: objLocalMapStats.getBackupEntryMemoryCost(),
					events: objLocalMapStats.getEventOperationCount(),
					hits: objLocalMapStats.getHits(),
					locks: objLocalMapStats.getLockedEntryCount(),
					dirty: objLocalMapStats.getDirtyEntryCount()
				};

				objHzClientInstance.shutdown();
				return res.json(values);
			})
			.catch(function(details) {
				sails.log('ERROR GOT CATCH GRACEFULLY YEAH!', details);
				return res.badRequest(details);
			});
	},

	pingMember: function(req, res) {

		sails.log('THE PATH', process.cwd());

		var response = {success: false};

		var HazelcastClientClass = java.import('com.hazelcast.client.HazelcastClient');
	  var ClientConfigClass = java.import('com.hazelcast.client.config.ClientConfig');
	  var ArrayListClass = java.import('java.util.ArrayList');

		var memberHostAddress = req.param('hostAddress');

		if (!memberHostAddress) {
			return res.badRequest(response);
		}

		var clientConfig = new ClientConfigClass();
		var addresses = new ArrayListClass();
		var networkConfig = clientConfig.getNetworkConfig();

		addresses.add(memberHostAddress);
		sails.log('THE ADDRESS', memberHostAddress);
		networkConfig.setAddresses(addresses);
		var hazelcastClient = HazelcastClientClass.newHazelcastClient(clientConfig);
		var name = hazelcastClient.getName();
		var map = hazelcastClient.getMap('default');
		// var keys = map.values();
		// var tmp = {'the values': keys.toArray()};
		var keys = map.keySet();
		var tmp = {'the keys': keys.toArray()};
		HazelcastClientClass.shutdown(hazelcastClient);

		return res.json({success: tmp});
	}
};
