/**
 * SessionController
 *
 * @description :: Server-side logic for managing sessions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 module.exports = {
  login: function(req, res) {
    return res.view({
      ngApp : 'app.session'
    });
  },

  authenticate: function(req, res) {
    var moment = require('moment');
    User.findOne({username: req.body.username})
      .then(function(user) {
        if (!user) {
          return res.json(404, {error: 'USER_NOT_FOUND'});
        }

        if(!user.verifyPassword(req.body.password)) {
          return res.json(403, {error: 'ACCESS_DENIED'});
        }

        cookie = req.cookies;
        sails.log(cookie);

        req.session.authenticated = true;
        req.session.user = user;

        if (cookie.length > 1) {
          tokenObject = JSON.parse(cookie.auth_token);
          JwtAuthToken.verify(tokenObject.token)
            .then(function(verified) {
              req.token = verified;
              next();

              return res.json(200, {authenticated: true, user: user, token: req.token});
            })
            .catch(function(err) {
              // generate another token
              var time = moment();
              return res.json(200, {authenticated: true, user: user, token: JwtAuthToken.issue({id: user.id + time})});
            });

        } else {
          var time = moment();
          return res.json(200, {authenticated: true, user: user, token: JwtAuthToken.issue({id: user.id + time})});
        }

      })
      .catch(function(err) {
        sails.log(err);
        return res.json(500, {error: err});
      });
  },

  logout: function(req, res) {
    req.session.destroy(function(err) {
      if (!req.xhr) {
        return res.redirect('/login');
      }

      return res.json(200, {message: 'LOGOUT_SUCCESS'});
    });
  },

  currentSession: function(req, res) {
    if (!req.session.user) {
      return res.json(403, {error: 'ACCESS_DENIED'});
    }

    return res.json(200, {user: req.session.user});
  }



};
