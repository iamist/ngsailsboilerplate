/**
 * ClusterMapController
 *
 * @description :: Server-side logic for managing Clustermaps
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var java = require('java'),
  Promise = require('bluebird'),
  Chance = require('chance'),
  debug = require('debug')('APP:ClusterMapController');

var addressFormat = function(address) {
  if (!address) return false;

  var addr = address.split(':');

  var data = {
    hostname: addr[0]
  };
  if (addr.length > 1) {
    _.merge(data, {
      port: addr[1]
    });
  }

  return data;
}

module.exports = {
  checkStatus: function(req, res) {
    var addrs = [];
    if (!req.param('member')) {
      return res.badRequest({
        details: 'HZ_MEMBER_ADDRESS_NOT_SPECIFIED'
      });
    }

    if (_.isArray(req.param('member'))) {
      addrs = _.map(req.param('member'), addressFormat);
    } else {
      addrs.push(addressFormat(req.param('member')));
    }

    var HazelcastClientClass = java.import(
      'com.hazelcast.client.HazelcastClient');
    var ClientConfigClass = java.import(
      'com.hazelcast.client.config.ClientConfig');
    var ArrayListClass = java.import('java.util.ArrayList');

    var clientConfig = new ClientConfigClass();
    var addresses = new ArrayListClass();
    var networkConfig = clientConfig.getNetworkConfig();

    _.forEach(addrs, function(address) {
      addresses.add(address.hostname + ':' + address.port);
    });

    networkConfig.setAddresses(addresses);

    var values = {
      entries: '--',
      entryMemory: '--',
      backups: '--',
      backupMemory: '--',
      events: '--',
      hits: '--',
      locks: '--',
      dirty: '--',
      status: ''
    };

    HazelcastClientClass.newHazelcastClientAsync(clientConfig, function(err,
      hazelcastClient) {
      if (err) {
        sails.log.debug('ERROR', err);
        return res.badRequest(_.merge(values, req.allParams(), {
          status: 'down'
        }));
      }

      hazelcastClient.getMapAsync('default', function(err, map) {
        sails.log.info('THE MAP VALUES', map.values().toArray());
        values.entries = map.values().size();

        map.getLocalMapStatsAsync(function(err, objLocalMapStats) {
          if (err) {
            return res.badRequest(err.message);
          }

          _.merge(values, {
            entryMemory: objLocalMapStats.getOwnedEntryMemoryCost(),
            backups: objLocalMapStats.getBackupCount(),
            backupMemory: objLocalMapStats.getBackupEntryMemoryCost(),
            events: objLocalMapStats.getEventOperationCount(),
            hits: objLocalMapStats.getHits(),
            locks: objLocalMapStats.getLockedEntryCount(),
            dirty: objLocalMapStats.getDirtyEntryCount()
          }, req.allParams(), {
            status: 'good'
          });

          return res.json(values);
        });

      });
    });
  },

  browse: function(req, res) {
    if (!req.param('id')) {
      return res.badRequest('CLUSTER_MAP_ID_IS_REQUIRED');
    }

    function getFormattedAddress(rs) {
      return new Promise(function(resolve, reject) {
        var data = [];
        if (!rs.members.length) {
          // return empty address
          return reject([])
        }
        _.forEach(rs.members, function(member) {
          data.push([member.hostname, member.port].join(':'));
        });

        return resolve(data);
      });
    }

    function newHazelcastClient(paramAddresses) {
      var HazelcastClientClass = java.import(
        'com.hazelcast.client.HazelcastClient');
      var ClientConfigClass = java.import(
        'com.hazelcast.client.config.ClientConfig');
      var ArrayListClass = java.import('java.util.ArrayList');

      var clientConfig = new ClientConfigClass();
      var addresses = new ArrayListClass();
      var networkConfig = clientConfig.getNetworkConfig();

      _.forEach(paramAddresses, function(addr) {
        addresses.add(addr);
      });
      
      networkConfig.setAddresses(addresses);
      return HazelcastClientClass
        .newHazelcastClientPromise(clientConfig)
        .then(function(hzClientInstance) {
          return hzClientInstance;
        });
    }
    
    function getHazelcastClientMap(hzClientInstance, mapName) {
      var _mapName = mapName ? mapName : 'default';
      return hzClientInstance
        .getMapPromise(_mapName)
        .then(function(map) {
          return map;
        });
    }
    
    function getValuesFromKeySet(arrKeySets, contextMap) {
      var data = [];
      _.forEach(arrKeySets, function(key) {
        var v = contextMap.getEntryView(key);
        data.push({
          cost_in_bytes: v.getCost(),
          creation_time: v.getCreationTime(),
          expiration_time: v.getExpirationTime(),
          hits: v.getHits(),
          key: v.getKey(),
          value: v.getValue(),
          version: v.getVersion(),
          last_access_time: v.getLastAccessTime(),
          last_stored_time: v.getLastStoredTime(),
          last_update_time: v.getLastUpdateTime(),
          ttl: v.getTtl()
        });
      });
      
      return Promise.resolve(data);
    }
    
    var hzClient, mapData;

    ClusterMap
    .findOne({
      id: req.param('id')
    })
    .populate('members')
    .then(function(rsData) {
      mapData = rsData
      return mapData;
    })
    .then(getFormattedAddress)
    .then(newHazelcastClient)
    .then(function(hzClientInstance) {
      hzClient = hzClientInstance;
      return getHazelcastClientMap(hzClientInstance, mapData.name);
    })
    .then(function(map) {
      var arrKeySets = map.keySet().toArray();
      
      if (req.param('search_key')) {
        var arrKeySets = [req.param('search_key')];
      }
      
      getValuesFromKeySet(arrKeySets, map)
      .then(function(data) {
        hzClient.shutdown();
        return res.json(data);
      });
    })
    .catch(function(details) {
      debug('ERROR', details);
      hzClient.shutdown();
      return res.badRequest(details.message);
    });

  },

  seed: function(req, res) {
    var HazelcastClientClass = java.import(
      'com.hazelcast.client.HazelcastClient');
    var ClientConfigClass = java.import(
      'com.hazelcast.client.config.ClientConfig');
    var ArrayListClass = java.import('java.util.ArrayList');
    var TimeUnit = java.import('java.util.concurrent.TimeUnit');

    var clientConfig = new ClientConfigClass();
    var addresses = new ArrayListClass();
    var networkConfig = clientConfig.getNetworkConfig();

    addresses.add('10.35.3.25:5701');
    addresses.add('10.35.3.25:5702');
    networkConfig.setAddresses(addresses);

    HazelcastClientClass.newHazelcastClientAsync(clientConfig, function(err,
      hazelcastClient) {
      if (err) {
        sails.log.debug('ERROR', err);
        return res.badRequest('unable to connect to cluster');
      }

      var seedData = function(map) {
        var chance = new Chance();
        for (var i, i = 0; i <= 50; i++) {
          map.putAsync(chance.word({
            syllables: 1
          }), chance.word({
            syllables: 3
          }), 1437598800, TimeUnit.SECONDS);
        }
        return true;
      }

      hazelcastClient.getMapAsync('default', function(err, map) {
        if (err) {
          sails.log.debug('ERROR MAP', err);
          return res.badRequest('unable to get map');
        }
        // seed data here
        async.waterfall([
          function(cb) {
            seedData(map);
            return cb(false, true);
          },
          function(result, cb) {
            map.valuesAsync(function(err, result) {
              if (err) {
                return cb(err, false);
              }

              return cb(false, result.toArray());
            });
          }
        ], function(err, result) {
          if (err) {
            return res.json('failed to seed data');
          }
          return res.json({
            seed: result
          });
        })
      });
    });
  }
};
