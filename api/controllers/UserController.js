/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 *              actions defined below are all override to blueprint actions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 module.exports = {
  create: function (req, res) {
    User.create(req.params.all())
      .then(function(created) {
        return res.json(200, created);
      })
      .catch(function(err) {
        return res.badRequest({message: err});
      });
  },

  findOne: function(req, res) {
    return res.json(200, {foo: 'bar', data: req.params.all()});
  },

  find: function(req, res) {
    User.find(req.params.all())
      .then(function(data) {
        return res.json(200, data);
      })
      .catch(function(err) {
        return res.badRequest(err);
      });
  }
};

