/**
 * CacheController
 *
 * @description :: Server-side logic for managing Caches
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var java = require('java'),
  debug = require('debug');

function getMapData(mapId) {
  return ClusterMap
    .findOne({id:mapId})
    .populate('members')
    .then(function(data) {
      return data;
    });
}
function getFormattedAddress(rs) {
  return new Promise(function(resolve, reject) {
    var data = [];
    if (!rs.members.length) {
      // return empty address
      return reject([])
    }
    _.forEach(rs.members, function(member) {
      data.push([member.hostname, member.port].join(':'));
    });

    return resolve(data);
  });
}

function newHazelcastClient(paramAddresses) {
  var HazelcastClientClass = java.import(
    'com.hazelcast.client.HazelcastClient');
  var ClientConfigClass = java.import(
    'com.hazelcast.client.config.ClientConfig');
  var ArrayListClass = java.import('java.util.ArrayList');

  var clientConfig = new ClientConfigClass();
  var addresses = new ArrayListClass();
  var networkConfig = clientConfig.getNetworkConfig();

  _.forEach(paramAddresses, function(addr) {
    addresses.add(addr);
  });
  
  networkConfig.setAddresses(addresses);
  return HazelcastClientClass
    .newHazelcastClientPromise(clientConfig)
    .then(function(hzClientInstance) {
      return hzClientInstance;
    });
}

function getHazelcastClientMap(hzClientInstance, mapName) {
  var _mapName = mapName ? mapName : 'default';
  return hzClientInstance
    .getMapPromise(_mapName)
    .then(function(map) {
      return map;
    });
}

function getValuesFromKeySet(arrKeySets, contextMap) {
  var data = [];
  _.forEach(arrKeySets, function(key) {
    var v = contextMap.getEntryView(key);
    data.push({
      cost_in_bytes: v.getCost(),
      creation_time: v.getCreationTime(),
      expiration_time: v.getExpirationTime(),
      hits: v.getHits(),
      key: v.getKey(),
      value: v.getValue(),
      version: v.getVersion(),
      last_access_time: v.getLastAccessTime(),
      last_stored_time: v.getLastStoredTime(),
      last_update_time: v.getLastUpdateTime(),
      ttl: v.getTtl()
    });
  });
  
  return Promise.resolve(data);
}

module.exports = {
  
  replace: function(req, res) {
    if (!req.param('cache_key')) {
      return res.badRequest('CACHE_KEY_REQUIRED');
    }
    
    if (!req.param('id')) {
      return res.badRequest('MAP_ID_REQUIRED')
    }
		
		var mapData, hzClientInstance;
		
		getMapData(req.param('id'))
		.then(function(data) {
			mapData = data;
			return data;
		})
		.then(getFormattedAddress)
		.then(newHazelcastClient)
		.then(function(hzClientInstance) {
			hzClientInstance = hzClientInstance;
			return getHazelcastClientMap(hzClientInstance, mapData.name);;
		})
		.then(function(map) {
			return map
				.replacePromise(req.param('cache_key'), req.param('cache_value'));
		})
		.then(function(result) {
			return res.json(result);
		})
		.catch(function(details) {
			debug('ERROR IN REMOVE', details);
			return res.badRequest(details.message);
		});
  },
  
  remove: function(req, res) {
    if (!req.param('cache_key')) {
      return res.badRequest('CACHE_KEY_REQUIRED');
    }
    
    if (!req.param('id')) {
      return res.badRequest('MAP_ID_REQUIRED')
    }
    
    var hzClientInstance, mapData;
    
    getMapData(req.param('id'))
    .then(function(data) {
      mapData = data;
      return data;
    })
    .then(getFormattedAddress)
    .then(newHazelcastClient)
    .then(function(hzClientInstance) {
      hzClientInstance = hzClientInstance;
      return getHazelcastClientMap(hzClientInstance, mapData.name);
    })
    .then(function(map) {
      map
      .removePromise(req.param('cache_key'));
    })
		.then(function(result) {
			return res.json(result);
		})
    .catch(function(details) {
      debug('ERROR IN DELETE', details);
      return res.badRequest(details.message);
    });
  },
  
  clear: function(req, res) {
    if (!req.param('id')) {
      return res.badRequest('MAP_ID_REQUIRED')
    }
  }
};
