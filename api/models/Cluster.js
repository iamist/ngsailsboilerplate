/**
* ClusterGroup.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  tableName: 'clusters',
  attributes: {
    name: {
      type: "string",
      required: true
    },
    description: {
      type: "text"
    },
    is_deleted: {
      type: 'boolean',
      defaultsTo: false
    },
    maps: {
      collection: 'ClusterMap',
      via: 'cluster'
    }
  }

};
