/**
* Session.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

var bcrypt = require('bcrypt'),
  uuid = require('node-uuid'),
  Promise = require('bluebird');

var SALT_WORK_FACTOR = 10;

// Promise.promisifyAll(bcrypt);
var genSalt = Promise.promisify(bcrypt.genSalt),
  genHash = Promise.promisify(bcrypt.hash);

module.exports = {
  attributes: {
    name: {
      type: 'string',
      required: true
    },

    username: {
      type: 'string',
      required: true,
      unique: true
    },

    password: {
      type: 'string',
      required: true,
      minLength: 6,
      maxLength: 20,
      columnName: 'encrypted_password'
    },
    
    isEnabled: {
      required: true,
      defaultsTo: true,
      type: 'boolean'
    },

    verifyPassword: function(password) {
      return bcrypt.compareSync(password, this.password);
    },

    toJSON: function() {
      var value = this.toObject();
      //delete value.password;
      return value;
    }

  },

  //@TODO: debug change password
  /*beforeUpdate: function(attrs, cb) {
    if (!attrs.password) return cb();

    genSalt(SALT_WORK_FACTOR)
      .then(function(salt) {
        genHash(attrs.password, salt)
          .then(function(hash) {
            attrs.password = hash;
            return cb();
          })
          .catch(function(err) {
            return cb(err);
          });
      })
      .catch(function(err) {
        return cb(err);
      });
  },*/

  beforeCreate: function(attrs, cb) {
    genSalt(SALT_WORK_FACTOR)
      .then(function(salt) {
        genHash(attrs.password, salt)
          .then(function(hash) {
            attrs.password = hash;
            return cb();
          })
          .catch(function(err) {
            return cb(err);
          });
      })
      .catch(function(err) {
        return cb(err);
      })
  }
};
