/**
* Cluster.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

var debug = require('debug')('APP:ClusterModel');

module.exports = {

  tableName: 'cluster_members',
  attributes: {
    name: {
      required: true,
      type: 'string',
      size: 60,
      unique: true
    },
    hostname: {
      required: true,
      type: 'string'
    },
    port: {
      required: true,
      type: 'string',
      unique: true
    },
    map: {
      model: 'ClusterMap',
      required: true
    }
  }
};
