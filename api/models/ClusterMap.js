/**
* ClusterMap.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  tableName: "cluster_maps",
  attributes: {
    name: {
      type: "string",
      required: true,
      unique: true
    },
    description: {
      type: "text"
    },
    cluster: {
      model: "Cluster",
      required: true
    },
    members: {
      collection: 'ClusterMember',
      via: 'map'
    }
  }
};
