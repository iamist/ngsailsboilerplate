/**
 * tokenAuth
 *
 * @module      :: Policy
 * @description :: Policy for authenticating user via json webtoken
 */

module.exports = function(req, res, next) {
  var token;

  if (req.headers && req.headers.authorization) {
    var parts = req.headers.authorization.split(' ');
    if (parts.length == 2) {
      var scheme = parts[0],
          credentials = parts[1];

      if (/^Bearer$/i.test(scheme)) {
        token = credentials;
      }
    }
  } else if (req.param('token')) {
    token = req.param('token');
    delete req.query.token;
  } else {
    return res.json(401, {error: 'AUTH_TOKEN_REQUIRED'});
  }
  console.log('WHAT IS THE TOKEN IN THE POLICY??', token);

  JwtAuthToken.verify(token)
    .then(function(verified) {
      req.token = verified;
      next();
    })
    .catch(function(err) {
      return res.json(403, {error: 'AUTH_TOKEN_VERIFICATION_FAILED', details: err});
    });
};
