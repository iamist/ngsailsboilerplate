describe('Account Menu Directive', function() {
	var elem, scope;

	// load are angular module
	// note that we use `angular.mock.module`
	// because `module` is a reserved node function
	beforeEach(angular.mock.module('app'));

	// load the templates we precompiled
	// via karma-nghtml2js-preprocessor
	beforeEach(angular.mock.module('Templates'));

	// create and compile the directive
	beforeEach(inject(function($compile, $rootScope) {
		mockScope = $rootScope.$new();
		mockElement = angular.element('<account-menu></account-menu>');
		$compile(mockElement)(mockScope);
		mockScope.$digest();
	}));

	it('should have the dashboard account menu', function() {
		var nav = mockElement.find('ul.navbar-top-links');
		expect(nav.length).toEqual(1);
	});

	it('should have user menu', function() {
		var userMenu = mockElement.find('ul li.dropdown ul.dropdown-user');
		expect(userMenu.length).toEqual(1);
		var children = userMenu.find('li');
		expect(children.length).toEqual(4);

		expect(angular.element(children[0]).text().trim()).toEqual('User Profile');
		expect(angular.element(children[1]).text().trim()).toEqual('Settings');
		expect(angular.element(children[2]).text().trim()).toEqual('');
		expect(angular.element(children[2]).attr('class')).toEqual('divider');
		expect(angular.element(children[3]).text().trim()).toEqual('Logout');

	});

});