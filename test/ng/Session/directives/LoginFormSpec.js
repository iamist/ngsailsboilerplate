describe('Login Form Directive', function() {
    var elem, scope;

    // load are angular module
    // note that we use `angular.mock.module`
    // because `module` is a reserved node function
    beforeEach(angular.mock.module('sessionModule'));

    // load the templates we precompiled
    // via karma-nghtml2js-preprocessor
    beforeEach(angular.mock.module('Templates'));

    // create and compile the directive
    beforeEach(inject(function($compile, $rootScope) {
        mockScope = $rootScope.$new();
        mockElement = angular.element('<login-form></login-form>');
        $compile(mockElement)(mockScope);
        mockScope.$digest();
    }));

    it('should contain username, password and submit field', function() {
        var username = mockElement.find('input#username');
        var password = mockElement.find('input#password');
        var submit = mockElement.find('button#btnSubmit');
        
        expect(angular.element(username).length).toEqual(1);
        expect(angular.element(password).length).toEqual(1);
        expect(angular.element(submit).length).toEqual(1);
    });

    it('should have a form validation', function() {
        var form  = mockElement.find('form[name=loginForm]');
        var submitEvent = spyOnEvent(form, 'submit');
        form.trigger('submit');

        expect('submit').toHaveBeenTriggeredOn(form);
        expect(submitEvent).toHaveBeenTriggered();

    });

});