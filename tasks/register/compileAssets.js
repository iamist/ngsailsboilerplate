module.exports = function (grunt) {
	grunt.registerTask('compileAssets', [
		'clean:dev',
		'jst:dev',
		'less:dev',
		'copy:bower',
		'copy:fonts',
		'copy:dev',
		'coffee:dev',
		'browserify'
	]);
};
