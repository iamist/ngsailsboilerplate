# ng-hazelcast

# dev setup

 1. Install NodeJs
 2. Install global packages (package list separated by <space>)

    `# npm i -g sails karma-cli bower grunt-cli nodemon`

 3. Clone the project

    `# git clone https://iamist@bitbucket.org/iamist/ngsailsboilerplate.git`

 4. Enter project root and install frontend and backend dependencies
 
    `# npm install`
    
    `# bower install`
 
 5. Run grunt tasks to build the frontend assets
    
    `# grunt build`

 6. Start the application
    
    `# npm start`    
 

# build

# deployment