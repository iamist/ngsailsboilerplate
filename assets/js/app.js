require('./ng/Common/modules/Layout');
require('./ng/Dashboard/modules/Dashboard');
require('./ng/Session/modules/Session');
require('./ng/User/modules/User');
require('./ng/Cluster/modules/Cluster');
require('./ng/Stats/modules/Stats');

var angular = require('angular'),
  uiRouter = require('uiRouter'),
  ngCookies = require('ngCookies'),
  _ = require('lodash'),
  angularMoment = require('ngMoment');

var appSession = angular.module('app.session', ['sessionModule', 'ui.router']);
    appSession.config(require('./ng/Session/config/Routes'));

var appDashboard = angular.module('app.dashboard', [
      'layoutModule',
      'sessionModule',
      'clusterModule',
      'dashboardModule',
      'userModule',
      'statsModule',
      'ui.router',
      'ngCookies',
      'angularMoment'
    ]);

appDashboard.config(['$urlRouterProvider', function($urlRouterProvider) {
  $urlRouterProvider.otherwise('/');
}]);
appDashboard.factory('AuthInterceptor', require('./ng/Session/services/AuthInterceptor'));
appDashboard.config(['$httpProvider', function($httpProvider) {
  $httpProvider.interceptors.push('AuthInterceptor');
}]);

appDashboard.run(['$window', '$rootScope', 'CLUSTER_EVENTS', function($window, $rootScope, CLUSTER_EVENTS) {

  $rootScope.alerts = [];
  $rootScope.closeAlert = function(index) {
    $rootScope.alerts.splice(index, 1);
  };
  $rootScope.quickSidebarNav = {};

  $rootScope.$bus.subscribe(CLUSTER_EVENTS.view_cluster_map, function(data) {
    _.map($rootScope.quickSidebarNav, function(nav) {
      nav.active = '';
    });
    $rootScope.quickSidebarNav[data.label] = _.merge(data, {active: 'active'});
  });

  //Loads the correct sidebar on window load,
  //collapses the sidebar on window resize.
  // Sets the min-height of #page-wrapper to window size
  $($window).on('load resize', function() {
    topOffset = 50;
    width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
    if (width < 768) {
      $('div.navbar-collapse').addClass('collapse');
      topOffset = 100; // 2-row-menu
    } else {
      $('div.navbar-collapse').removeClass('collapse');
    }

    height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
    height = height - topOffset;
    if (height < 1) height = 1;
    if (height > topOffset) {
      $("#page-wrapper").css("min-height", (height) + "px");
    }
  });

}]);
