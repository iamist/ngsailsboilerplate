var Routes = ['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $stateProvider
      .state('dashboard', {
        url: '/',
        name: 'dashboard',
        templateUrl: '/js/ng/Dashboard/templates/dashboard.html',
        controller: 'DashboardController',
        resolve: {
          addedClusters: ['$rootScope', 'ClusterService', 'CLUSTER_EVENTS',
            function($rootScope, ClusterService, CLUSTER_EVENTS) {
              return ClusterService
                .getClusters({populate: ['maps']})
                .then(function(response) {
                  if (!response.data) {
                    return [];
                  }
                  return response.data;
                })
            }
          ]
        }
      });
  }];

  module.exports = Routes;
