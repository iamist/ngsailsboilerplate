var angular = require('angular'),
  uiRouter = require('uiRouter');

var Dashboard = angular.module('dashboardModule', ['ui.router']);

Dashboard.controller('DashboardController', require('../controllers/DashboardController'));

Dashboard.config(require('../config/Routes'));

module.exports = Dashboard;
