var _ = require('lodash');
var DashboardController = ['$scope', 'AUTH_EVENTS', 'AuthService', '$window', 'AuthService', 'addedClusters',
  function($scope, AUTH_EVENTS, AuthService, $window, AuthService, addedClusters) {

    $scope.$bus.subscribe(AUTH_EVENTS.logoutSuccess, function(res) {
        $window.location = '/login';
    });

    $scope.$bus.subscribe(AUTH_EVENTS.logoutFailed, function(err) {
        console.log('ERROR ON LOGOUT', err);
    });

    AuthService.currentSession().then(function(user) {
      console.log('the current user is ', user);
    });

    $scope.clusters = _.chunk(addedClusters, 3);

    $scope.logout = function() {
        AuthService
            .logout()
            .then(function(res) {
                $scope.$bus.publish(AUTH_EVENTS.logoutSuccess, res);
            })
            .catch(function(err) {
                $scope.$bus.publish(AUTH_EVENTS.logoutFailed, err);
            });
    };
}];

module.exports = DashboardController;
