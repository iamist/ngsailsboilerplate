var Routes = ['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

  $stateProvider
    .state('user', {
      url: '/user',
      name: 'user',
      views: {
        '': {
          templateUrl: '/js/ng/User/templates/user.html',
          controller: 'UserController'
        },
        'Menu@user': {
          controller: 'DashboardController',
          templateUrl: '/js/ng/Common/templates/menu.html'
        },
        'Content@user': {
          controller: 'UserController',
          templateUrl: '/js/ng/User/templates/profile.html'
        }
      }
    })

    .state('user.addUser', {
        url: '/add',
        name: 'add-user',
        views: {
            '': {
                templateUrl: '/js/ng/User/templates/user.html',
                controller: 'UserController'
            },
            'Menu@user': {
                controller: 'DashboardController',
                templateUrl: '/js/ng/Common/templates/menu.html'
            },
            'Content@user': {
                controller: 'UserController',
                templateUrl: '/js/ng/User/templates/add-user.html'
            }
        }
    })

    .state('user.editUser', {
        url: '/edit/:id',
        name: 'edit-user',
        views: {
            '': {
                controller: 'UserController',
                templateUrl: '/js/ng/User/templates/user.html'
            },
            'Menu@user': {
                controller: 'DashboardController',
                templateUrl: '/js/ng/Common/templates/menu.html'
            },
            'Content@user': {
                controller: 'UserController',
                templateUrl: '/js/ng/User/templates/edit-user.html'
            }
        }
    })
}];

module.exports = Routes;
