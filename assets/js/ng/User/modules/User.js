var angular = require('angular'),
  uiRouter = require('uiRouter');

require('../../Common/modules/MessageBus');

var UserModule = angular.module('userModule', ['MessageBusModule', 'ui.router']);

UserModule.controller('UserController', require('../controllers/UserController'));

UserModule.config(require('../config/Routes'));

module.exports = UserModule;
