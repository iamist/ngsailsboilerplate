var StatsController = [
  '$rootScope',
  '$scope',
  '$timeout',
  'CLUSTER_EVENTS',
  'ClusterSession',
  'StatsService',
  'cluster',
  '$modal',
  function($rootScope, $scope, $timeout, CLUSTER_EVENTS, ClusterSession, StatsService, cluster, $modal) {
    $scope.stats = false;
    $scope.clusterInfo = cluster;
    $scope.tabs = [
      {title: "Default", content: "/js/ng/Cluster/templates/map-memory-data-table.html"}
    ];

    $scope.openMapBrowser = function() {
      var modalInstance = $modal.open({
        templateUrl: '/js/ng/Cluster/templates/map-browser-modal.html',
        controller: ['$scope', '$modalInstance', function($scope, $modalInstance) {
          $scope.cancel = function() {
            $modalInstance.close();
          }
        }]
      });
    }

    $scope.openCreateMapModal = function() {
      var modalInstance = $modal.open({
        templateUrl: '/js/ng/Cluster/templates/create-map-form-modal.html',
        controller: ['$scope', '$modalInstance', function($scope, $modalInstance) {

          $scope.createMap = function() {
            $modalInstance.close();
          }

          $scope.cancel = function() {
            $modalInstance.close()
          }
        }]
      });
    }

  }
];

module.exports = StatsController;
