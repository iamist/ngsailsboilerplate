var angular = require('angular'),
  uiRouter = require('uiRouter');

var StatsModule = angular.module('statsModule', ['ui.router', 'ui.bootstrap']);

StatsModule.service('StatsService', require('../services/StatsService'));
StatsModule.directive('serverStatus', require('../directives/serverStatus'));
StatsModule.directive('memoryDistribution', require('../directives/memoryDistribution'));
StatsModule.directive('cacheSize', require('../directives/cacheSize'));
StatsModule.directive('getStats', require('../directives/getStats'));
StatsModule.directive('setStats', require('../directives/setStats'));
StatsModule.directive('deleteStats', require('../directives/deleteStats'));
StatsModule.directive('incrementStats', require('../directives/incrementStats'));
StatsModule.directive('decrementStats', require('../directives/decrementStats'));
StatsModule.directive('flushStats', require('../directives/flushStats'));
StatsModule.directive('casStats', require('../directives/casStats'));
StatsModule.directive('cacheSizeGraph', require('../directives/cacheSizeGraph'));
StatsModule.directive('hitMissRateGraph', require('../directives/hitMissRateGraph'));
StatsModule.controller('StatsController', require('../controllers/StatsController'));

module.exports = StatsModule;
