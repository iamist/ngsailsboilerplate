var StatsService = ['$rootScope', '$http', '$q', 'ClusterSession', function($rootScope, $http, $q, ClusterSession) {
  return {
    /*getMemoryDistribution: function() {
      var deferred = $q.defer();
      return $http
        .get('/api/v/0.0.1/stats/memoryDistribution/' + ClusterSession.currentWatchedCluster().id);
    },

    getCacheSize: function() {
      return $http
        .get('/api/v/0.0.1/stats/cacheSize/' + ClusterSession.currentWatchedCluster().id);
    },

    getServerStatus: function() {
      return $http
        .get('/api/v/0.0.1/stats/serverStatus/' + ClusterSession.currentWatchedCluster().id)
    },*/

    statistics: function() {
      return $http
        .get('/api/v/0.0.1/stats/statistics/' + ClusterSession.currentWatchedCluster().id);
    }
  }
}];

module.exports = StatsService;
