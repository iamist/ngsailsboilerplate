var ServerStatus = ['$rootScope', function($rootScope) {
  return {
    restrict: 'AE',
    templateUrl: '/js/ng/Stats/templates/server-status.html'
  }
}];

module.exports = ServerStatus;
