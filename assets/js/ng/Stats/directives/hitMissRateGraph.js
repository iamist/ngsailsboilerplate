var $ = require('jquery'),
  Flot = require('Morris');

var hitMissRateGraph = ['$rootScope', '$timeout', '$window', function($rootScope, $timeout, $window) {
  return {
    restrict: 'AE',
    templateUrl: '/js/ng/Stats/templates/hit-miss-rate-graph.html',
    link: function(scope, elem, attr) {
      scope.$watch('stats.hitMissRate', function(newVal) {
        if (newVal) {

        }
      });

      var data = [[1, 81.7], [2, 18.20]],
          options = {
            xaxis: {ticks: [[1,"Hits"], [2,"Miss"]]},
            resize: true
          };

      var drawPlot = function() {
        $.plot('#' + elem.find('.bar-graph').attr('id'), [{
          data: data,
          color: "#454d7d",
          bars: {
            show: true,
            fill: 1,
            fillColor: "#757dad",
            align: "center"
          }
        }], options);
      }

      drawPlot();
      $window.onresize = function(evt) {
        drawPlot();
      }
    }
  }
}];

module.exports = hitMissRateGraph;
