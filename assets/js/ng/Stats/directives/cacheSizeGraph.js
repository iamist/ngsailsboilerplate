var $ = require('jquery'),
  Flot = require('Flot'),
  FlotPie = require('Flot.Pie');

var cacheSizeGraph = ['$rootScope', '$timeout', '$window', function($rootScope, $timeout, $window) {
  return {
    restrict: 'AE',
    templateUrl: '/js/ng/Stats/templates/cache-size-graph.html',
    link: function(scope, elem, attrs) {
      var data;
      var drawPlot = function() {
        $.plot('#' + elem.find('.pie-graph').attr('id'), data, {
          series: {
            pie: {
              show: true
            }
          }
        });
      };

      scope.$watch('stats.cacheSize', function(newVal) {
        if (newVal) {
          data = newVal;
          drawPlot();
        }
      });

      $window.onresize = function() {
        drawPlot();
      }
    }
  }
}];

module.exports = cacheSizeGraph;
