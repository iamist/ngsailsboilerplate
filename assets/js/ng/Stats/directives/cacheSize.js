var cacheSize = ['$rootScope', '$timeout', function($rootScope, $timeout) {
  return {
    restrict: 'AE',
    templateUrl: '/js/ng/Stats/templates/cache-size.html'
  }
}];

module.exports = cacheSize;
