var flushStats = ['$rootScope', '$window', function($rootScope, $window) {
  return {
    templateUrl: '/js/ng/Stats/templates/flush-stats.html'
  }
}];

module.exports = flushStats;
