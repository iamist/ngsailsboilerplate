var MemoryDistribution = [
    '$rootScope',
    '$timeout',
    '$window',
    'ClusterSession',
    'CLUSTER_EVENTS',
    function($rootScope, $timeout, $window, ClusterSession, CLUSTER_EVENTS) {
      return {
        restrict: 'AE',
        templateUrl: '/js/ng/Stats/templates/memory-distribution.html'
      }
    }
];
module.exports = MemoryDistribution;
