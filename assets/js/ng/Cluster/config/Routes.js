var Routes = ['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

  $stateProvider
    .state('cluster', {
      abstract: true,
      controller: 'ClusterController',
      templateUrl: '/js/ng/Cluster/templates/index.html'
    })
    .state('cluster.cluster_list', {
      url: '/clusters',
      templateUrl: '/js/ng/Cluster/templates/cluster-list.html',
      controller: 'ClusterController'
    })
    .state('cluster.cluster_details', {
      url: '/clusters/:clusterId',
      templateUrl: '/js/ng/Cluster/templates/cluster-details.html',
      controller: 'ClusterController'
    })
    .state('cluster.cluster_map', {
      url: '/clusters/:clusterId/map/:mapId',
      templateUrl: '/js/ng/Cluster/templates/cluster-map-details.html',
      controller: 'ClusterMapController'
    })

    /* everything belore is for admin routing */
    .state('admin_cluster', {
      abstract: true,
      name: 'admin_cluster',
      templateUrl: '/js/ng/Cluster/templates/admin/index.html'
    })
    .state('admin_cluster.list', {
      url: '/admin/clusters',
      name: 'admin_cluster.list',
      views: {
        '': {
          templateUrl: '/js/ng/Cluster/templates/admin/cluster-list.html',
          controller: 'AdminClusterController'
        }
      }
    })
    .state('admin_cluster.new_cluster', {
      url: '/admin/clusters/new_cluster',
      name: 'admin_cluster_new',
      templateUrl: '/js/ng/Cluster/templates/admin/new-cluster.html',
      controller: 'AdminClusterController'
    })
    .state('admin_cluster.edit_cluster', {
      url: '/admin/clusters/edit_cluster/:clusterId',
      name: 'admin_cluster_edit',
      templateUrl: '/js/ng/Cluster/templates/admin/edit-cluster.html',
      controller: 'AdminClusterController'
    })

    /* cluster members */
    .state('admin_cluster.members', {
      url: '/admin/cluster/members',
      parent: 'admin_cluster',
      views: {
        '': {
          templateUrl: '/js/ng/Cluster/templates/admin/member-list.html',
          controller: 'AdminClusterMemberController'
        }
      }
    })
    .state('admin_cluster.new_member', {
      url: '/admin/cluster/new-member',
      templateUrl: '/js/ng/Cluster/templates/admin/new-member.html',
      controller: 'AdminClusterMemberController'
    })
    .state('admin_cluster.edit_member', {
      url: '/admin/cluster/edit-member/:memberId',
      templateUrl: '/js/ng/Cluster/templates/admin/edit-member.html',
      controller: 'AdminClusterMemberController'
    })

    /* cluster maps */
    .state('admin_cluster.maps', {
      url: '/admin/cluster/maps',
      parent: 'admin_cluster',
      views: {
        '': {
          templateUrl: '/js/ng/Cluster/templates/admin/map-list.html',
          controller: 'AdminClusterMapController'
        }
      }
    })
    .state('admin_cluster.new_map', {
      url: '/admin/cluster/new-map',
      templateUrl: '/js/ng/Cluster/templates/admin/new-map.html',
      controller: 'AdminClusterMapController'
    })
    .state('admin_cluster.edit_map', {
      url: '/admin/cluster/edit-map/:mapId',
      templateUrl: '/js/ng/Cluster/templates/admin/edit-map.html',
      controller: 'AdminClusterMapController'
    });

}];

module.exports = Routes;
