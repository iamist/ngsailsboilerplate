var angular = require('angular'),
  uiRouter = require('uiRouter'),
  uiBootstrap = require('uiBootstrap');

var ClusterModule = angular.module('clusterModule', [
      'ui.router',
      'ui.bootstrap'
    ]);

ClusterModule.constant('CLUSTER_EVENTS', require('../constant/ClusterEvents'));
ClusterModule.factory('ClusterService', require('../services/ClusterService'));
ClusterModule.service('ClusterSession', require('../services/ClusterSession'));
ClusterModule.controller('ClusterController', require('../controllers/ClusterController'));
ClusterModule.controller('ClusterMapController', require('../controllers/ClusterMapController'));
ClusterModule.controller('MapBrowserController', require('../controllers/MapBrowserController'));
ClusterModule.controller('FlushMapController', require('../controllers/FlushMapController'));
ClusterModule.controller('AdminClusterController', require('../controllers/admin/ClusterController'));
ClusterModule.controller('AdminClusterMemberController', require('../controllers/admin/ClusterMemberController'));
ClusterModule.controller('AdminClusterMapController', require('../controllers/admin/ClusterMapController'));
ClusterModule.directive('clusterConnectForm', require('../directives/clusterConnectForm'));
ClusterModule.directive('clusterConnectModal', require('../directives/clusterConnectModal'));
ClusterModule.directive('clusterList', require('../directives/clusterList'));

ClusterModule.config(require('../config/Routes'));

module.exports = ClusterModule;
