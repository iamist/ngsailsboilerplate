var ClusterList = ['$rootScope', '$timeout', function ($rootScope, $timeout) {
  return {
    require: '?ngModel',
    restrict: 'AE',
    link: function(scope, elem, attrs) {

    },
    templateUrl: '/js/ng/Cluster/templates/cluster-list.html'
  }
}];

module.exports = ClusterList;
