var $ = require('jquery');

var ClusterConnectModal = ['$rootScope', '$timeout', function($rootScope, $timeout) {
  return {
    restrict: 'AE',
    scope: {
      clusters: "=clusters"
    },
    link: function(scope, elem, attrs) {
      //@TODO: use $watch???
      if (!scope.clusters.length) {
        $timeout(function() {
          $('#clusterConnectModal').modal('show');
        });
      }
    },
    templateUrl: '/js/ng/Cluster/templates/cluster-connect-modal.html'
  }
}];

module.exports = ClusterConnectModal;
