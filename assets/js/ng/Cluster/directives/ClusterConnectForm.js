var $ = require('jquery');

var ClusterConnectForm = ['$rootScope', '$window', '$timeout', function($rootScope, $window, $timeout) {
  return {
    restrict: 'AE',
    link: function(scope, elem, attrs) {
      var $clusters = elem.find('[name=selClusters]'),
        $openFields = elem.find('.open-fields');

      $clusters.on('change', function(e) {
        var self = this;
        if (self.value) {
          $openFields.find('input').attr('disabled', true);
        } else {
          $openFields.find('input').removeAttr('disabled');
        }
      });
    },
    templateUrl: '/js/ng/Cluster/templates/cluster-connect-form.html'
  }
}];

module.exports = ClusterConnectForm;
