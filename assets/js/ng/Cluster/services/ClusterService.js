var $ = require('jquery');

var ClusterService = ['$http', '$q', function($http, $q) {
  var api = '/api/v/0.0.1';
  return {

    getClusters: function(opt) {
      var defaults = $.extend({populate: false}, opt);
      return $http
        .get(api + '/cluster?' + $.param(defaults));
    },

    createCluster: function(params) {
      return $http
        .post(api + '/cluster/create', params);
    },

    getCluster: function(id, opt) {
      var defaults = $.extend({populate: false}, opt);
      return $http
        .get(api + '/cluster/' + id + '/?' + $.param(defaults));
    },

    updateCluster: function(params) {
      var id = params.id;
      delete params.id;
      return $http
        .put(api + '/cluster/update/' + id, params);
    },

    destroyCluster: function(id) {
      return $http
        .get(api + '/cluster/destroy/' + id);
    },

    /* members api */
    getClusterMembers: function() {
      return $http
        .get(api + '/clustermember?populate=cluster');
    },

    getClusterMember: function(id, parent) {
      if (!parent) {
        populate = false;
      } else {
        populate = parent.join(',');
      }

      return $http
        .get(api + '/clustermember/' + id + '?populate=' + populate );
    },

    createClusterMember: function(params) {
      return $http
        .post(api + '/clustermember', params)
    },

    destroyClusterMember: function(memberId) {
      return $http
        .get(api + '/clustermember/destroy/' + memberId);
    },

    updateClusterMember: function(params) {
      var memberId = params.id;
      delete params.id;
      return $http
        .post(api + '/clustermember/update/' + memberId, params)
    },

    /* cache data related API calls */
    getMemberStatus: function(member) {
      var id = member.id || undefined;
      var params = {
        member: member ? [member.hostname, member.port].join(':') : null
      }
      return $http
        .post(api + '/clustermap/checkStatus/' + id, params);
    },
    
    getMapCacheData: function(mapId) {
      return $http.get(api + '/clustermap/browse/' + mapId);
    },

    /* maps api */
    getClusterMaps: function() {
      return $http
        .get(api + '/clustermap?populate=cluster');
    },

    getClusterMap: function(id, opt) {
      var defaults = $.extend({populate: false}, opt);
      return $http
        .get(api + '/clustermap/' + id + '/?' + $.param(defaults));
    },

    createClusterMap: function(params) {
      return $http
        .post(api + '/clustermap', params)
    },

    destroyClusterMap: function(memberId) {
      return $http
        .get(api + '/clustermap/destroy/' + memberId);
    },

    updateClusterMap: function(params) {
      var memberId = params.id;
      delete params.id;
      return $http
        .post(api + '/clustermap/update/' + memberId, params)
    }

  }
}];

module.exports = ClusterService;
