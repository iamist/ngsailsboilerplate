var _ = require('lodash');

var ClusterSession = [
    '$rootScope',
    '$q',
    '$http',
    function($rootScope, $http, $q) {

      this.watchCluster = {};

      this.addToWatch = function(cluster) {
        this.watchCluster[cluster.id] = cluster;
      }

      this.currentWatchedCluster = function() {
        return _.find(this.watchCluster, {isCurrentView: true});
      }

    }
];

module.exports = ClusterSession;
