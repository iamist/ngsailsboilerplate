var $ = require('jquery'),
  _ = require('lodash');

var ClusterController = [
  '$rootScope',
  '$scope',
  '$stateParams',
  '$modalInstance',
  'CLUSTER_EVENTS',
  'ClusterSession',
  'ClusterService',
  'clusterInfo',
  function ($rootScope, $scope, $stateParams, $modalInstance,
    CLUSTER_EVENTS, ClusterSession, ClusterService, clusterInfo) {

  $scope.clusterDetails = clusterInfo;

  $scope.closeMapBrowser = function() {
    $modalInstance.close();
  }

}];

module.exports = ClusterController;
