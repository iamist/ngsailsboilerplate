var _ = require('lodash');

var ClusterController = ['$rootScope', '$scope', '$state', '$stateParams', 'ClusterService', 'CLUSTER_EVENTS',
  function($rootScope, $scope, $state, $stateParams, ClusterService, CLUSTER_EVENTS) {

    $scope.clusterMembers = [];
    $scope.member_form = {
      name: '',
      description: ''
    };
    $scope.clusters = [];

    $scope.getClusters = function() {
      ClusterService
        .getClusters()
        .then(function(response) {
          if (!response.data) {
            $state.go('admin_cluster.list');
            $rootScope.alerts.push({type:'danger', msg: 'You need to add cluster data.'});
          }

          $scope.clusters = response.data;
        })
    }

    $scope.getMembers = function() {
      ClusterService
        .getClusterMembers()
        .then(function(response) {
          if (response.data) {
            $scope.clusterMembers = response.data;
          }
        });
    }

    $scope.createMember = function() {
      ClusterService
        .createClusterMember($scope.member_form)
        .then(function(response) {
          if (response.data) {
            $state.transitionTo('admin_cluster.members');
            $rootScope.alerts.push({type: 'success', msg: 'Record successfully added.'});
          }
        })
        .catch(function(details) {
          $rootScope.alerts.push({type:'danger', msg: 'Oops! Something went wrong, please try again later.'});
        });
    }

    $scope.editMember = function() {
      if ($stateParams.memberId) {
        ClusterService
          .getClusterMember($stateParams.memberId)
          .then(function(response) {
            if (response.data) {
              $scope.member_form = response.data;
            }
          })
          .catch(function(details) {
            $rootScope.alerts.push({type:'danger', msg: 'Oops! Something went wrong, please try again later.'});
          });
      }
    }

    $scope.updateMember = function() {
      ClusterService
        .updateClusterMember($scope.member_form)
        .then(function(response) {
          if (response.data) {
            $rootScope.alerts.push({type: 'success', msg: 'Record successfully updated.'});
            $state.transitionTo('admin_cluster.members');
          }
        })
        .catch(function(details) {
          $rootScope.alerts.push({type:'danger', msg: 'Oops! Something went wrong, please try again later.'});
        });
    }

    $scope.deleteMember = function(id) {
      ClusterService
        .destroyClusterMember(id)
        .then(function(response) {
          if (response.data) {
            if ($scope.clusterMembers.length) {
              $rootScope.alerts.push({type: 'info', msg: 'Record successfully added.'});
              $scope.clusterMembers.splice(_.findKey($scope.clusterMembers, {id: response.data.id}), 1);
            }
          }
        })
        .catch(function(details) {
          $rootScope.alerts.push({type:'danger', msg: 'Oops! Something went wrong, please try again later.'});
        });
    }

    $scope.cancelMemberForm = function() {
      $state.go('admin_cluster.members');
    }

}];

module.exports = ClusterController;
