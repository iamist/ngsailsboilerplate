var _ = require('lodash');

var ClusterMapController = ['$rootScope', '$scope', '$state', '$stateParams', 'ClusterService', 'CLUSTER_EVENTS',
  function($rootScope, $scope, $state, $stateParams, ClusterService, CLUSTER_EVENTS) {

    $scope.clusterMaps = [];
    $scope.map_form = {
      name: '',
      description: ''
    };
    $scope.clusters = [];

    $scope.getClusters = function() {
      ClusterService
        .getClusters({populate: false})
        .then(function(response) {
          if (!response.data) {
            $state.go('admin_cluster.list');
            $rootScope.alerts.push({type:'danger', msg: 'You need to add cluster data.'});
          }

          $scope.clusters = response.data;
        })
    }

    $scope.getMaps = function() {
      ClusterService
        .getClusterMaps()
        .then(function(response) {
          if (response.data) {
            $scope.clusterMaps = response.data;
          }
        });
    }

    $scope.createMap = function() {
      ClusterService
        .createClusterMap($scope.map_form)
        .then(function(response) {
          if (response.data) {
            $state.transitionTo('admin_cluster.maps');
            $rootScope.alerts.push({type: 'success', msg: 'Record successfully added.'});
          }
        })
        .catch(function(details) {
          $rootScope.alerts.push({type:'danger', msg: 'Oops! Something went wrong, please try again later.'});
        });
    }

    $scope.editMap = function() {
      if ($stateParams.mapId) {
        ClusterService
          .getClusterMap($stateParams.mapId)
          .then(function(response) {
            if (response.data) {
              $scope.map_form = response.data;
            }
          })
          .catch(function(details) {
            $rootScope.alerts.push({type:'danger', msg: 'Oops! Something went wrong, please try again later.'});
          });
      }
    }

    $scope.updateMap = function() {
      ClusterService
        .updateClusterMap($scope.map_form)
        .then(function(response) {
          if (response.data) {
            $rootScope.alerts.push({type: 'success', msg: 'Record successfully updated.'});
            $state.transitionTo('admin_cluster.maps');
          }
        })
        .catch(function(details) {
          $rootScope.alerts.push({type:'danger', msg: 'Oops! Something went wrong, please try again later.'});
        });
    }

    $scope.deleteMap = function(id) {
      ClusterService
        .destroyClusterMap(id)
        .then(function(response) {
          if (response.data) {
            if ($scope.clusterMaps.length) {
              $rootScope.alerts.push({type: 'info', msg: 'Record successfully added.'});
              $scope.clusterMaps.splice(_.findKey($scope.clusterMaps, {id: response.data.id}), 1);
            }
          }
        })
        .catch(function(details) {
          $rootScope.alerts.push({type:'danger', msg: 'Oops! Something went wrong, please try again later.'});
        });
    }
}];

module.exports = ClusterMapController;
