var _ = require('lodash');

var ClusterController = ['$rootScope', '$scope', '$state', '$stateParams', 'ClusterService', 'CLUSTER_EVENTS',
  function($rootScope, $scope, $state, $stateParams, ClusterService, CLUSTER_EVENTS) {

    $scope.clusters = [];
    $scope.cluster_form = {
      name: '',
      description: ''
    };

    $scope.getClusters = function() {
      ClusterService
        .getClusters()
        .then(function(response) {
          if (response.data) {
            $scope.clusters = response.data;
          }
        });
    }

    $scope.createCluster = function() {
      ClusterService
        .createCluster($scope.cluster_form)
        .then(function(response) {
          if (response.data) {
            $state.transitionTo('admin_cluster.list');
            $rootScope.alerts.push({type: 'success', msg: 'Record successfully added.'});
          }
        })
        .catch(function(details) {
          $rootScope.alerts.push({type:'danger', msg: 'Oops! Something went wrong, please try again later.'});
        });
    }

    $scope.editCluster = function() {
      if ($stateParams.clusterId) {
        ClusterService
          .getCluster($stateParams.clusterId)
          .then(function(response) {
            if (response.data) {
              $scope.cluster_form = response.data;
            }
          })
          .catch(function(details) {
            $rootScope.alerts.push({type:'danger', msg: 'Oops! Something went wrong, please try again later.'});
          });
      }
    }

    $scope.updateCluster = function() {
      ClusterService
        .updateCluster($scope.cluster_form)
        .then(function(response) {
          if (response.data) {
            $rootScope.alerts.push({type: 'success', msg: 'Record successfully updated.'});
            $state.transitionTo('admin_cluster.list');
          }
        })
        .catch(function(details) {
          $rootScope.alerts.push({type:'danger', msg: 'Oops! Something went wrong, please try again later.'});
        });
    }

    $scope.deleteCluster = function(id) {
      ClusterService
        .destroyCluster(id)
        .then(function(response) {
          if (response.data) {
            if ($scope.clusters.length) {
              $scope.clusters.splice(_.findKey($scope.clusters, {id: response.data.id}), 1);
              $rootScope.alerts.push({type: 'info', msg: 'Record successfully added.'});
            }
          }
        })
        .catch(function(details) {
          $rootScope.alerts.push({type:'danger', msg: 'Oops! Something went wrong, please try again later.'});
        });
    }

    $scope.cancelClusterForm = function() {
      $state.go('admin_cluster');
    }

}];

module.exports = ClusterController;
