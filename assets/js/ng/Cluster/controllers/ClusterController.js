var $ = require('jquery'),
  _ = require('lodash');

var ClusterController = [
  '$rootScope',
  '$scope',
  '$stateParams',
  '$modal',
  'CLUSTER_EVENTS',
  'ClusterSession',
  'ClusterService',
  function ($rootScope, $scope, $stateParams, $modal,
    CLUSTER_EVENTS, ClusterSession, ClusterService) {

  var getClusterMaps = function(clusterId) {
    return ClusterService
      .getCluster(clusterId, {populate:'maps'})
      .then(function(response) {
        if (!response.data) return false;

        $scope.clusterDetails = response.data;
        return response.data;
      });
  },
  getMapMembers = function(mapId) {
    return ClusterService
      .getClusterMap(mapId, {populate:'members'})
      .then(function(response) {
        if (!response.data) false;
        return response.data;
      })
  };

  $scope.clusterDetails = {};
  $scope.mapMembers = {};

  $scope.init = function() {
    getClusterMaps($stateParams.clusterId)
      .then(function(cluster) {
        var firstMap = !cluster ? false : cluster.maps[0];
        if (firstMap) {
          $scope.mapMembers[firstMap.id] = getMapMembers(firstMap.id);
        }
      })
  }

  $scope.openMapBrowser = function() {
    var modalInstance = $modal.open({
      templateUrl: '/js/ng/Cluster/templates/map-browser-modal.html',
      controller: 'MapBrowserController',
      resolve: {
        clusterInfo: function() {
          return {id: $scope.clusterDetails.id, name: $scope.clusterDetails.name};
        }
      }
    });
  }

  $scope.confirmFlushModal = function() {
    var modalInstance = $modal.open({
      templateUrl: '/js/ng/Cluster/templates/flush-map-modal.html',
      controller: 'FlushMapController',
      resolve: {
        clusterInfo: function() {
          return {id: $scope.clusterDetails.id, name: $scope.clusterDetails.name};
        }
      }
    });
  }

}];

module.exports = ClusterController;
