var $ = require('jquery'),
  _ = require('lodash');

var ClusterMapController = [
  '$rootScope',
  '$scope',
  '$q',
  '$stateParams',
  '$modal',
  'CLUSTER_EVENTS',
  'ClusterSession',
  'ClusterService',
  function ($rootScope, $scope, $q, $stateParams, $modal,
    CLUSTER_EVENTS, ClusterSession, ClusterService) {

      var memberStatsPromises = [];
      $scope.clusterMapData = {};

      function getMapData(mapId) {
        return ClusterService
          .getClusterMap(mapId, {populate:['members']})
          .then(function(response) {
            if (!response.data) {
              return false;
            }
            $scope.clusterMapData = response.data;
            return response.data;
          });
      }

      function getMemberStats(mapData) {
        return _.map(mapData.members, function(member) {
          memberStatsPromises.push(ClusterService.getMemberStatus(member));
        });
      }

      function getMapCacheKeys(map) {

      }

      function displayMemberStats(member) {
        _.extend(_.find($scope.clusterMapData.members, {id: +member.data.id}), {stats: member.data});
      }

      $scope.init = function() {
          getMapData($stateParams.mapId)
            .then(function(mapData) {
              _.forEach(mapData.members, function(member) {
                ClusterService
                  .getMemberStatus(member)
                  .then(displayMemberStats)
                  .catch(displayMemberStats);
              });

              // add history sidebar nav
              $scope.$bus.publish(CLUSTER_EVENTS.view_cluster_map, {
                state: 'cluster.cluster_map({clusterId:' + $scope.clusterMapData.cluster + ',mapId:' + $scope.clusterMapData.id + '})',
                icon: 'database',
                label: 'Map: ' + $scope.clusterMapData.name
              });

            });
      }
      
    $scope.browse = function() {
      ClusterService
      .getMapCacheData($stateParams.mapId)
      .then(function(response) {
        if (response.data) {
          $scope.clusterMapData.map = response.data;
        }
      })
      .catch(function(details) {
        console.log('FETCH_MAP_DATA_ERROR', details.message);
      });
    }

}];

module.exports = ClusterMapController;
