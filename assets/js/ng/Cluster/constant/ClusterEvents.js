module.exports = {
  clusterListEmpty: 'EMPTY_CLUSTER_LIST',
  watch_current_cluster: 'LOAD_AND_WATCH_CURRENT_CLUSTER',
  watch_cluster_not_found: 'CLUSTER_TO_WATCH_NOT_FOUND',
  error_get_memory_distribution: 'ERROR_GETTING_MEM_DISTRIBUTION',
  new_cluster_group_created: 'CLUSTER_GROUP_CREATED',
  cluster_group_deleted: 'CLUSTER_GROUP_DELETED',
  new_cluster_created: 'CLUSTER_CREATED',
  cluster_deleted: 'CLUSTER_DELETED',
  view_cluster_map: 'VIEW_CLUSTER_MAP'
}
