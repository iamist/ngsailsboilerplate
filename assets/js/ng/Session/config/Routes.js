var Routes = ['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  
  // default
  $urlRouterProvider.otherwise('/');

  $stateProvider.state('login', {
    url: '/',
    name: 'session.login',
    controller: 'AuthController'
  });

}];

module.exports = Routes;