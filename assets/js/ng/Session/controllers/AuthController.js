var AuthController = ['$scope', 'AuthService', 'AUTH_EVENTS', '$window', function($scope, AuthService, AUTH_EVENTS, $window) {
	$scope.credentials = {
		username: '',
		password: ''
	};

  $scope.$bus.subscribe(AUTH_EVENTS.loginSuccess, function(response) {
    $window.location = '/dashboard';
  });

  $scope.$bus.subscribe(AUTH_EVENTS.loginFailed, function(error) {
        // @TODO: add flash message or whatever
      });

  $scope.login = function() {
    AuthService
    .authenticate($scope.credentials)
    .then(function(response) {
      $scope.$bus.publish(AUTH_EVENTS.loginSuccess, response);
    })
    .catch(function(error) {
      $scope.$bus.publish(AUTH_EVENTS.loginFailed, error);
    });
  }

  $scope.logout = function() {
    AuthService
    .logout()
    .then(function(response) {
      $scope.$bus.publish(AUTH_EVENTS.logoutSuccess, response);
    })
    .catch(function(err) {
      $scope.$bus.publich(AUTH_EVENTS.logoutFailed, err);
    });
  }

}];

module.exports = AuthController;
