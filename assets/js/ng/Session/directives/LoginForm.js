var LoginForm = ['$rootScope', '$window', '$timeout', function($rootScope, $window,  $timeout) {
  return {
    restrict: 'AE',
    scope: true, // will create a child scope that will prototypically inherit from its parent
    link: function(scope, elem, attrs) {
      // @TODO: clien-side validation
    },
    templateUrl: '/js/ng/Session/templates/login-form.html'
  }
}];


module.exports = LoginForm;