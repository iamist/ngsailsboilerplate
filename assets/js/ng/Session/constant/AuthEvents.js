module.exports = {
  loginSuccess: 'AUTH_LOGIN_SUCCESS',
  loginFailed: 'AUTH_LOGIN_FAILED',
  logoutSuccess: 'AUTH_LOGOUT_SUCCESS',
  logoutFailed: 'AUTH_LOGOUT_FAILED',
  sessionTimeout: 'AUTH_SESSION_TIMEOUT',
  unAuthorized: 'AUTH_NOT_AUTHORIZED'
};
