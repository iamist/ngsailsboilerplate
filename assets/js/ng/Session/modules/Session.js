var angular = require('angular'),
  ngCookies = require('ngCookies');

require('../../Common/modules/MessageBus');

var Session = angular.module('sessionModule', [
      'MessageBusModule',
      'ngCookies'
    ]);

Session.provider('SessionProvider', require('../services/SessionProvider'));

// setup constants
Session.constant('AUTH_EVENTS', require('../constant/AuthEvents'));

// setup sesison provider
Session.config(['SessionProviderProvider', function(SessionProviderProvider) {
}]);

Session.factory('JwtService', require('../services/JwtService'));

Session.factory('AuthService', require('../services/AuthService'));

Session.directive('loginForm', require('../directives/LoginForm'));

Session.controller('AuthController', require('../controllers/AuthController'));

module.exports = Session;
