var SessionProvider = function() {

    this.currentSession = {};

    return {

        create: function(user) {
            currentSession = user;
        },

        destroy: function() {
            currentSession = null;
        },

        $get: ['$rootScope', function sessionFactory($rootScope) {
                return {
                  create: this.create,
                  destroy: this.destroy,
                  currentSession: this.currentSession
                };
            }]
    };
};

module.exports = SessionProvider;
