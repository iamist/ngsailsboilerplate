var AuthInterceptor = ['$q', 'JwtService', '$injector', function($q, JwtService, $injector) {
  return {
    request: function(config) {
      var token;
      if (JwtService.get('auth_token')) {
        token = angular.fromJson(JwtService.get('auth_token')).token;
      }

      if(token) {
        config.headers.Authorization = ['Bearer', token].join(' ');
      }

      return config;
    },
    responseError: function(response) {
      if (response.status === 401 || response.status === 403) {
        //JwtService.destroy('auth_token');
        $injector.get('$state').go('login.modal');
      }
      return $q.reject(response);
    }
  }
}];

module.exports = AuthInterceptor;
