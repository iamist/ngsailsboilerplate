var AuthService = ['$http', '$q', 'JwtService', function($http, $q, JwtService) {

  return {
    authenticate: function(credentials) {
      var deferred = $q.defer();
      return $http
        .post('/session/authenticate', credentials)
        .success(function(data) {
          // get the token and save to cookie
          JwtService.set({token: data.token});
          deferred.resolve(data);
        })
        .error(function(error) {
          deferred.reject(error);
        });

        return deferred.promise;
    },

    currentSession: function() {
      var deferred = $q.defer();
      return $http
        .get('/session/current')
        .success(function(data) {
          deferred.resolve(data);
        })
        .error(function(err) {
          deferred.reject(err);
        })

        return deferred.promise;
    },

    logout: function() {
      var deferred = $q.defer();

      return $http
        .get('/session/logout')
        .success(function(data) {
          JwtService.destroy();
          deferred.resolve(data);
        })
        .error(function(err) {
          deferred.reject(err);
        });

        return deferred.promise();
    },

    isAuthenticated: function() {

    },

    isAuthorized : function() {
    }
  };

}];

module.exports = AuthService;
