var JwtService = ['$cookies', function($cookies) {
  return {
    set: function(obj) {
      $cookies.putObject('auth_token', obj);
    },

    get: function(key) {
      var k = key || 'auth_token';
      return $cookies.get(k);
    },

    destroy: function(key) {
      var k = key || 'auth_token';
      return $cookies.remove(k);
    }
  }
}];

module.exports = JwtService;
