var metisMenu = require('metisMenu');
var SidebarMenu = ['$rootScope', '$window', '$timeout', function($rootScope, $window, $timeout) {
	return {
		restrict: 'AE',
		link: function(scope, elem, attrs) {
			elem.find('#side-menu').metisMenu();
		},
		templateUrl: '/js/ng/Common/templates/sidebar-menu.html'
	}
}];

module.exports = SidebarMenu;