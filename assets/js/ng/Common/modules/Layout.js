var angular = require('angular');

var Layout = angular.module('layoutModule', []);

Layout.directive('sidebarMenu', require('../directives/SidebarMenu'));
Layout.directive('accountMenu', require('../directives/AccountMenu'));

module.exports = Layout;