// Karma configuration
// Generated on Tue Jun 23 2015 10:47:03 GMT+0800 (China Standard Time)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['browserify', 'jasmine'],

    // browserify preprocessors
    browserify: {
        debug: true,
        transform: ['browserify-shim']
    },


    // list of files / patterns to load in the browser
    files: [
        'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js',
        'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js',
        'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css',
        'https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.1/angular.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.2.15/angular-ui-router.min.js',
        'node_modules/angular-mocks/angular-mocks.js',
        'assets/js/importer.js',
        'bower_components/metisMenu/dist/*{.min.js,min.css}',
        'node_modules/jasmine-jquery/lib/jasmine-jquery.js',
        // templates
        'assets/js/ng/**/templates/*.html',
        'test/**/*Spec.js'
    ],


    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
        'assets/js/importer.js': ['browserify'],
        'test/**/*.js': ['browserify'],
        'assets/js/ng/**/templates/*.html' : ['ng-html2js']
    },

    // enable accessing templates by injecting it as module
    ngHtml2JsPreprocessor: {
        moduleName: 'Templates',
        cacheIdFromPath : function(filepath) {
            // by default karma will use path relative to disk
            // so we need to strip the prefix path/directory 
            // to make the url relative to the host url
            // this is to allow the script we wrote to access 
            // js templates as if it was visited by users
            return filepath.substr(6, filepath.length);
        }
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['spec'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false
});
};
